from django.contrib import admin
from .models import *

class ProjectFolderAdmin(admin.ModelAdmin):
    raw_id_fields = ('projects',)
admin.site.register(ProjectFolder,ProjectFolderAdmin)

class CaptureProjectAdmin(admin.ModelAdmin):
    readonly_fields = ('last_edit',)
admin.site.register(CaptureProject,CaptureProjectAdmin)

class CaptureRateAdmin(admin.ModelAdmin):
    pass
admin.site.register(Rate,CaptureRateAdmin)