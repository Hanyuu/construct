# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.views.generic import View
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from .models import ProjectFolder

class ConsView(View):
    def get(self, request):
        user = request.user

        ProjectFolder.objects.get_or_create(user_id=168, human_name=u'Мои Проекты',type=1)
 
        return render_to_response('construct.html',{'request':request}, RequestContext(request))
