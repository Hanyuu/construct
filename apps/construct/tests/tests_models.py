from django.test import TestCase
from apps.construct.models import CaptureProject,ProjectFolder

class CaptureTestCase(TestCase):
    USER_ID = 100659#GORP
    def setUp(self):
        CaptureProject.objects.create(project_name='Test Project',json_structure='[]')
        CaptureProject.objects.create(project_name='Test Project2',json_structure='[]')
        ProjectFolder.objects.create(user_id=self.USER_ID,human_name='Test Folder')


    def test_construct(self):
        capture = CaptureProject.objects.get(project_name='Test Project')
        capture2 = CaptureProject.objects.get(project_name='Test Project2')
        folder = ProjectFolder.objects.get(user_id=self.USER_ID)
        folder.projects.add(capture)
        folder.projects.add(capture2)
        self.assertEquals(folder.projects.all().count(),2)
        self.assertEquals(folder.user_id,self.USER_ID)
        self.assertFalse(capture.is_published)
        capture.create_file('test','<html>TEST</html>')
        self.assertTrue(capture.is_published)
        capture.del_file()
        self.assertFalse(capture.is_published)
        capture2.create_file('test2','<html>TEST2</html>')
        self.assertTrue(capture2.is_published)
        capture2.delete()


