# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('construct', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='rate',
            options={'verbose_name': 'Рейт', 'verbose_name_plural': 'Рейты'},
        ),
        migrations.AlterField(
            model_name='captureproject',
            name='json_structure',
            field=models.CharField(max_length=100000, verbose_name='JSON содержимое'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='projectfolder',
            name='projects',
            field=models.ManyToManyField(null=True, to='construct.CaptureProject', blank=True, related_name='projects'),
            preserve_default=True,
        ),
    ]
