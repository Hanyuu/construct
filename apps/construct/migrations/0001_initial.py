# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CaptureProject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('project_name', models.CharField(verbose_name='Название проекта', max_length=100)),
                ('server_name', models.CharField(verbose_name='Серверное имя', blank=True, max_length=100)),
                ('json_structure', models.CharField(verbose_name='JSON содержимое', max_length=10000)),
                ('last_edit', models.DateTimeField(verbose_name='Дата последнего изменения', auto_now=True)),
                ('visit_count', models.IntegerField(verbose_name='Количество посещений', default=0)),
            ],
            options={
                'verbose_name': 'Проект',
                'verbose_name_plural': 'Проекты',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProjectFolder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('user_id', models.IntegerField()),
                ('human_name', models.CharField(verbose_name='Название папки', max_length=100)),
                ('type', models.SmallIntegerField(choices=[[0, 'private'], [1, 'eternal'], [2, 'shared']], default=0)),
                ('projects', models.ManyToManyField(related_name='projects', to='construct.CaptureProject')),
            ],
            options={
                'verbose_name': 'Папка проекта',
                'ordering': ('type',),
                'verbose_name_plural': 'Папки проектов',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Rate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('alias', models.CharField(verbose_name='Rate Alias', unique=True, max_length=100, help_text='Только на английском!')),
                ('capture_pages', models.PositiveIntegerField(verbose_name='Всего страниц')),
                ('capture_published_pages', models.PositiveIntegerField(verbose_name='Всего опубликованных страниц')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
