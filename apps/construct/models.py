# -*- coding: utf-8 -*-
from django.db import models
from ..utils.disk import write_html, delete_from_disk
from .exceptions import ProjectNotPublished

import logging
logger = logging.getLogger(__name__)

CHOICES_FOR_FOLDER=(
    [0,'private'],
    [1,'eternal'],
    [2,'shared'],

)

class CaptureProject(models.Model):

    project_name = models.CharField(u'Название проекта',max_length=100)

    server_name = models.CharField(u'Серверное имя',max_length=100,blank=True)

    json_structure = models.CharField(u'JSON содержимое',max_length=100000)

    last_edit = models.DateTimeField(u'Дата последнего изменения',auto_now=True)

    visit_count = models.IntegerField(u'Количество посещений',default=0)


    @property
    def is_published(self):
        return len(self.server_name) > 0

    def create_file(self,server_name,html):
        try:
            user_id = ProjectFolder.objects.filter(projects__in=[self])[0].user_id
            user = check_user_by_id(user_id)
            success,path=write_html(user['username'].lower(),server_name,html)
            if success:
                self.server_name = server_name
                self.save()
                return path
            else:
               logger.warning(path)
        except Exception as e:
            logger.warning(e)

    def del_file(self):
        if not self.is_published:
            raise ProjectNotPublished()
        else:
            try:
                user_id = ProjectFolder.objects.filter(projects__in=[self])[0].user_id
                user = check_user_by_id(user_id)
                success,path=delete_from_disk(user['username'],self.server_name)
                if success:
                    self.server_name=''
                    self.save()
                    return path
                else:
                    logger.warning(path)
            except Exception as e:
                logger.warning(e)

    def delete(self,simple_del=False, using=None):
        pass


    def __str__(self):
        return self.project_name

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'



class ProjectFolder(models.Model):

    user_id = models.IntegerField(unique=False)

    human_name = models.CharField('Название папки',max_length=100)

    projects = models.ManyToManyField(CaptureProject,related_name='projects',null=True,blank=True)

    type = models.SmallIntegerField(choices=CHOICES_FOR_FOLDER,default=0)

    def __str__(self):
        return self.human_name

    def simple_projects_data(self):
        return self.projects.all().values('id','project_name','last_edit','server_name')

    class Meta:
        verbose_name = 'Папка проекта'
        verbose_name_plural = 'Папки проектов'
        ordering = ('type',)

class Rate(models.Model):

    alias = models.CharField('Rate Alias', max_length=100, unique=True, blank=False, null=False,help_text='Только на английском!')

    capture_pages = models.PositiveIntegerField('Всего страниц')
    capture_published_pages = models.PositiveIntegerField('Всего опубликованных страниц')

    def __str__(self):
        return self.alias

    class Meta:
        verbose_name = 'Рейт'
        verbose_name_plural = 'Рейты'

