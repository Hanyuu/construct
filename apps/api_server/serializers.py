from rest_framework import serializers
import logging
from ..construct.models import CaptureProject, ProjectFolder

logger = logging.getLogger(__name__)

class CaptureProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaptureProject
        fields = ('id','project_name','server_name','json_structure','last_edit','visit_count',)

class FolderSimpleSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectFolder
        fields = ('id','human_name','type',)

class FolderFullSerializer(serializers.ModelSerializer):
    projects = serializers.Field(source='simple_projects_data')
    class Meta:
        model = ProjectFolder
        fields = ('id','human_name','projects','type')

