from rest_framework import routers
from django.conf.urls import url,patterns
router = routers.SimpleRouter()
from .views import *

router.register(r'^api/v1/get/folders/by/user/(?P<uid>[\d]+)', FolderSimpleView)

router.register(r'^api/v1/get/folder/(?P<fid>[\d]+)', FolderView)

router.register(r'^api/v1/create/folder', FolderView)
router.register(r'^api/v1/rename/folder', FolderView)
router.register(r'^api/v1/delete/folder', FolderView)
router.register(r'^api/v1/move/folder/project', FolderView)

router.register(r'^api/v1/get/projects/by/folder/(?P<uid>[\d]+)', ProjectView)
router.register(r'^api/v1/save/project', ProjectView)

router.register(r'^api/v1/delete/projects', DeleteProjectView)

router.register(r'^api/v1/rename/project', RenameProjectView)

router.register(r'^api/v1/open/project/(?P<pid>[\d]+)', GetProjectView)

router.register(r'^api/v1/get/published/created', GetPublishedCreatedView)

api_patterns = patterns('',
    url(r'^api/v1/update/visit/count',update,name='update-counter')
)




