from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated,AllowAny
from django.http.response import HttpResponse,HttpResponseForbidden
from django.db.models import Q
import json
import logging
import platform
import traceback
from ..construct.models import CaptureProject, ProjectFolder
from .serializers import FolderFullSerializer, CaptureProjectSerializer, FolderSimpleSerializer
from ..utils.decorators import try_except_decorator
from ..utils.opgm_generator import generate_image_from_html

logger = logging.getLogger(__name__)

class ApiView(viewsets.ModelViewSet):

    renderer_classes = [JSONRenderer]
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]

class FolderSimpleView(ApiView):
    model = ProjectFolder
    model_serializer_class = FolderSimpleSerializer

    @try_except_decorator(logger, {"status": "error"})
    def list(self, request, uid, *args, **kwargs):
        folders = ProjectFolder.objects.filter(Q(user_id=uid)|Q(type=2))
        data = self.model_serializer_class(folders,many=True).data
        return Response(data,status.HTTP_200_OK)

class FolderView(ApiView):
    model = ProjectFolder
    model_serializer_class = FolderFullSerializer

    @try_except_decorator(logger, {"status": "error"})
    def list(self, request, fid, *args, **kwargs):
        folder = ProjectFolder.objects.get(pk=fid)
        if folder.user_id != request.user.id and folder.type != 2:
            return Response({},status.HTTP_403_FORBIDDEN)
        else:
            data = self.model_serializer_class(folder).data
            return Response(data,status.HTTP_200_OK)

    @try_except_decorator(logger, {"status": "error"})
    def create(self, request, *args, **kwargs):
        data = request.DATA
        user_id = request.user.id
        action = data.get('action')
        #Create Folder
        if action == 'create':
            folder_name = data.get('folder_name')
            exist = ProjectFolder.objects.filter(user_id=user_id,human_name=folder_name).count() > 0
            if exist:
                 return Response({'status':'exists'},status.HTTP_200_OK)
            else:
                folder = ProjectFolder.objects.create(user_id=user_id,human_name=folder_name)
                return Response({'status':'created','id':folder.id},status.HTTP_200_OK)
        #Rename Folder
        elif action == 'rename':
            folder_name = data.get('folder_name')
            folder_id = data.get('folder_id')
            folder = ProjectFolder.objects.get(user_id=user_id,pk=folder_id)
            if folder.type != 0:
                return Response({'status':'cannot-rename'},status.HTTP_200_OK)
            else:
                folder.human_name = folder_name
                folder.save()
                return Response({'status':'ok'},status.HTTP_200_OK)
        #Delete Folder
        elif action == 'delete':
            folder_id = data.get('folder_id')
            try:
                folder = ProjectFolder.objects.get(user_id=user_id,pk=folder_id)
            except ProjectFolder.DoesNotExist:
                return Response({'status':'cannot-delete'},status.HTTP_200_OK)
            if folder.type != 0:
                return Response({'status':'cannot-delete'},status.HTTP_200_OK)
            else:
                folder.delete()
                return Response({'status':'ok'},status.HTTP_200_OK)
        #Move Project across Folders
        elif action == 'move-project-folder':
            folder_id = data.get('folder_id')
            project_id = data.get('project_id')
            project = CaptureProject.objects.filter(pk=project_id)

            if project.count() == 0:
                return Response({'status':'cannot-move'},status.HTTP_200_OK)
            else:
                project = project[0]
                folder = ProjectFolder.objects.filter(user_id=user_id,pk=folder_id)
                if folder.count() == 0:
                    return Response({'status':'cannot-move'},status.HTTP_200_OK)
                else:
                    folder = folder[0]
                    old_folder = ProjectFolder.objects.filter(projects__in=[project_id])
                    if old_folder.count() == 0:
                        return Response({'status':'cannot-move'},status.HTTP_200_OK)
                    else:
                        old_folder = old_folder[0]
                        old_folder.projects.remove(project)
                        folder.projects.add(project)
                        return Response({'status':'moved'},status.HTTP_200_OK)


class ProjectView(ApiView):
    model = CaptureProject
    model_serializer_class = CaptureProjectSerializer

    @try_except_decorator(logger, {"status": "error"})
    def list(self, request, uid, *args, **kwargs):
        folders = ProjectFolder.objects.filter(user_id=uid)
        projects = folders.projects.all()
        data = self.model_serializer_class(projects,many=True).data
        return Response(data,status.HTTP_200_OK)

    @try_except_decorator(logger, {"status": "error"})
    def create(self, request, *args, **kwargs):

        rate = request.session['rate']
        user_id = request.user.id
        data = request.DATA
        folder_id = data.get('folder')
        json = data.get('json')
        name = data.get('name')
        html = data.get('html')
        logger.debug(user_id, name)
        server_name = data.get('server_name')
        folder, id, capture_project = (None,None,None)

        diskResult = 'ok'
        path = ''
        try:
            folder = ProjectFolder.objects.get(user_id=user_id,pk=folder_id)
        except ProjectFolder.DoesNotExist:
            folder = ProjectFolder.objects.filter(user_id=user_id).order_by('-pk')[0]
        capture = folder.projects.all().filter(project_name=name)
        if len(capture) == 0:
            if folder.projects.all().count() >= rate.capture_pages:
                return Response({'status':'error','detail':'max-pages-pablished'},status.HTTP_200_OK)
            else:
                project, created = CaptureProject.objects.get_or_create(project_name=name)
                project.json_structure=json
                project.save()
                capture_project = project
                id = project.pk
                folder.projects.add(project)
        else:
            capture = capture[0]
            capture.json_structure=json
            capture.save()
            capture_project = capture
            id = capture.pk
        if html and server_name:
            path = capture_project.create_file(server_name,html)

            if path is not None:
                diskResult = 'ok'
                path = path.replace('//index.html','')
                if platform.system() == 'Linux':
                    generate_image_from_html.delay(path=path)
                else:
                    generate_image_from_html(path=path)
            else:
                diskResult = 'failed'
        return Response({'status':'ok','id':id,'diskResult':diskResult,'path':path},status.HTTP_200_OK)

class DeleteProjectView(ApiView):
    model = CaptureProject
    model_serializer_class = CaptureProjectSerializer

    def list(self, request, *args, **kwargs):
        return Response({},status.HTTP_403_FORBIDDEN)

    @try_except_decorator(logger, {"status": "error"})
    def create(self, request, *args, **kwargs):
        projects_ids = request.DATA.getlist('projects[]')
        action = request.DATA.get('action')
        user_id = request.user.id
        deleted = {}
        if action is not None:
            projects = self.model.objects.filter(pk__in=projects_ids)
            for cap in projects:
                pk = cap.pk
                try:
                    folder = ProjectFolder.objects.get(projects__in=[cap])
                    if folder.user_id == user_id and folder.type != 3:

                        if action == 'delete-projects':
                            cap.delete(simple_del=(not cap.is_published))
                        elif action == 'delete-files' and cap.is_published:
                            cap.del_file()
                        deleted.update({pk:'deleted'})
                    else:
                        deleted.update({pk:'cannot-delete'})
                except ProjectFolder.MultipleObjectsReturned:
                    logger.warning(cap)
                return Response(deleted,status.HTTP_200_OK)


class GetProjectView(ApiView):
    model = CaptureProject
    model_serializer_class = CaptureProjectSerializer

    @try_except_decorator(logger, {"status": "error"})
    def list(self, request,pid, *args, **kwargs):
        capture = CaptureProject.objects.get(pk=pid)
        data = self.model_serializer_class(capture).data
        folder = ProjectFolder.objects.filter(projects__in=[pid])[0]
        data['folder']=folder.id
        return Response(data,status.HTTP_200_OK)





class RenameProjectView(ApiView):
    model = CaptureProject
    model_serializer_class = CaptureProjectSerializer

    def list(self, request, *args, **kwargs):
        return Response({},status.HTTP_403_FORBIDDEN)


    @try_except_decorator(logger, {"status": "error"})
    def create(self, request, *args, **kwargs):
        project_id = request.DATA.get('project')
        name = request.DATA.get('new_name')
        user_id = request.user.id
        project = self.model.objects.get(pk=project_id)
        folder = ProjectFolder.objects.get(projects__in=[project])
        if folder.user_id == user_id and folder.type != 1:
            project.project_name = name
            project.save()
            return Response({'status':'renamed'},status.HTTP_200_OK)
        else:
            return Response({'status':'cannot-rename'},status.HTTP_200_OK)

@try_except_decorator(logger, {"status": "error"})
def update(request, *args, **kwargs):
        page = request.GET.get('page')
        user_id = request.GET.get('user_id')
        callback = request.GET.get('callback')
        try:
            folder = ProjectFolder.objects.get(user_id=user_id,projects__project_name=page)
            capture = folder.projects.all().get(project_name=page)
            capture.visit_count += 1
            capture.save()
            data = '%s(%s)'%(callback,json.dumps({'count':capture.visit_count}))
            return HttpResponse(data, "text/javascript")
        except:
            return HttpResponseForbidden(traceback.print_exc())

class GetPublishedCreatedView(ApiView):
    model = CaptureProject

    @try_except_decorator(logger, {"status": "error"})
    def list(self, request, *args, **kwargs):
        user_id = request.user.id

        captures = self.model.objects.filter(projects__user_id=user_id)
        not_published = captures.filter(server_name='')
        is_published = captures.exclude(pk__in=not_published)
        return Response({'published':is_published.count(),'not_published':not_published.count()+is_published.count()})


