# -*- coding: utf-8 -*-
import time
import traceback
from datetime import datetime
from functools import wraps, partial

from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib import messages

def try_except_decorator(logger, error_result=None):
    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except Exception:
                logger.warn(traceback.format_exc())
                return error_result
        return wrapper
    return decorator