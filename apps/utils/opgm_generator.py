import logging
from subprocess import Popen
from django.conf import settings
from django_rq import job

logger = logging.getLogger(__name__)
class WkhtmlToImageException(Exception):
    pass

@job
def generate_image_from_html(path):
    try:
        logger.debug('%s %s'%(settings.PATH_TO_WKHTMLIMAGE,path))
        p = Popen([settings.PATH_TO_WKHTMLIMAGE,'--quality','50', '%s/%s'%(path.lower(),'index.html'),  '%s/%s'%(path.lower(),'site.png')])
        p.communicate()
        return True
    except FileNotFoundError:
        raise WkhtmlToImageException('wkhtmltoimage not installed!')
    except Exception as e:
        logger.warning(e)
        return False
