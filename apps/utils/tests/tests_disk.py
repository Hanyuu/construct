from django.test import TestCase
from apps.utils.disk import write_html, delete_from_disk, get_path
from apps.utils.opgm_generator import generate_image_from_html

class DiskTestCase(TestCase):
    users = ['gorp',]
    names = ['test',]
    def setUp(self):
        pass

    def test_disk(self):
        for name in self.users:
            for capture in self.names:
                self.assertEqual(write_html(name,capture,'<html><h1>%s</h1><html>'%capture)[0],True)
                self.assertTrue(generate_image_from_html(get_path(name,capture)),True)
                self.assertEqual(delete_from_disk(name,capture)[0],True)

