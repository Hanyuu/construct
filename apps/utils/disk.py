import  os
from django.conf import settings
import logging
logger = logging.getLogger(__name__)

def get_path(user,capture):
    path = settings.DISK_PATH_TEMPLATE%{'hosting':settings.HOSTING_PATH,
    'domain':settings.HOSTING_DOMAIN,'user':user,'capture':capture}
    return path

def get_index(path):
    return '%s/%s'%(path,'index.html')


def create_folder(user,capture):
    path = get_path(user,capture)
    if os.path.isdir(path):
        return path
    else:
        os.makedirs(path,exist_ok=True)
        return path

def delete_from_disk(user,capture):
    try:
       path_folder = get_path(user,capture)
       path_file = get_index(path_folder)
       if os.path.exists(path_folder):
           os.remove(path_file)
           os.remove(path_file.replace('index.html','site.png'))
           os.rmdir(path_folder)
           return (True, path_folder)
       else:
           return (False, path_folder)
    except Exception as e:
        logger.warning(e)
        return (False,e)

def write_html(user,capture,html):
    try:
        path = create_folder(user,capture)
        path = get_index(path)
        index = open(path,'w+')
        index.write(html)
        index.close()
        return (True,path)
    except Exception as e:
        logger.warning(e)
        return (False, e)




