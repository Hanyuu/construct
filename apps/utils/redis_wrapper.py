# -*- coding: utf-8 -*-

from django.conf import settings
import redis
import logging

logger = logging.getLogger(__name__)

def get_redis():

    return redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB,password=settings.REDIS_PASSWORD)


