'use strict'
var GUI=new function(){
    var _instance;
    function GUI(){
        if(!_instance){
            var self=this;
            this.id=new Date().getTime();
            this.currentProject={};
            this.body=$(document.body);
            this.panel_content=$('.panel-content');
            this.user_id=undefined;
            this.user_name = undefined;
            this.csrf=$("[name='csrfmiddlewaretoken']").val();
            this.anchors={};

            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    xhr.setRequestHeader("X-CSRFToken", self.csrf);
                }
            });
            this.updateInfo();
            EventEmitter.getInstance().on('contentAdded', function(data){
                EventEmitter.getInstance().emit('contentChanged');
               Geometry.contentElements[data.el.id]=data.el;
            });
        
            EventEmitter.getInstance().on('contentDeleted', function(data){
                 delete Geometry.contentElements[data.id];
            });
            _instance=this;
        }
        else{
            return _instance;
        }

}   /**
     *Return GUI
     */
    GUI.getInstance=function(){
        return new GUI;
    };
    /**
     * Will call API (api/v1/get/published/created) and get or update  data about user projects
     */
    GUI.prototype.updateInfo=function(){
        $.getJSON('/api/v1/get/published/created/',{},function(data){
            $('#saved-projects').text(data.not_published);
            $('#published-projects').text(data.published);
        });
    };
    /**
     *  Will show  link for current project
     */
    GUI.prototype.showLink=function(){
        var current=$('.current');
        if(!this.currentProject.project_name&&(current.attr('id')==undefined||current.find('.server-name').text().length == 0)){
            this.showMessage('Вы должны открыть проект.');
        }
        else{
            var form=$('#Modal_ask_user')
                ,project_name;
            project_name=this.currentProject.server_name!=undefined?this.currentProject.server_name:current.find('.server-name').text()
            form.find('#question').text('Ссылка на страницу');
            form.find('#input_name').children().val('http://'+this.user_name+'-'+project_name+'.ajetlp.com');
            form.modal('show');
            form.fb('.good','click',function(){
                form.modal('hide');
            });
        }
    };
    /**
     *  Create new template
     */
    GUI.prototype.createNew=function(){
        var self=this;
        this.showQuestion('Вы действительно хотите создать новый, пустой шаблон? Все не сохранённые данные будут потеряны!', function(){
            self.clearField(false);
        }); 
    };
    /**
     *  Will publish project 
     */
    GUI.prototype.publishProject=function(){
        var self=this;
        if(!self.currentProject.project_name){
              self.showMessage('Для публикации вы должны сохранить проект.');
        }else if(self.currentProject.server_name){
            self.showQuestion('Вы действительно хотите перезаписать страницу '+self.currentProject.server_name+' ?','Перезапись проекта',function(){
                self.saveProject({
                    humanName:self.currentProject.project_name,
                    folder:self.currentProject.folder, 
                    exists:true, 
                    server_name:self.currentProject.server_name
                });
            })
        }else{
            var form=$('#Modal_ask_user');
            form.find('#question').text('Введите имя страницы')
            //Publish project, after all passed
            var publish=function(name){
                self.saveProject({
                    humanName:self.currentProject.project_name,
                    folder:self.currentProject.folder, 
                    exists:true, 
                    server_name:name,
                    callback:function(){
                        form.modal('hide');
                        if(self.currentProject.server_name){
                            self.showLink();
                        }
                    }
                });
            };
            form.fb('.good','click',function(){
                if(self.currentProject.server_name){
                    self.showQuestion('Вы действительно хотите перезаписать страницу '+self.currentProject.server_name+' ?','Перезапись проекта',publish);
                }else{
                    var name=form.find('#input_name').children().val();
                    if(/^[a-z0-9]{4,20}$/.test(name)){
                        publish(name);
                    }else{
                        self.showMessage('Имя может состоять только из английских букв или цифр и иметь длинну от 4х до 20 символов','alert-danger');
                    }
                }
            });
            form.modal('show')
        }
    };
    /***
     *
     * @param id
     * RenameProject
     */
    GUI.prototype.renameProject=function(id){
        var self=this;
        this.askUser('Введите новое название проекта',true,function(new_name){
            $.post('/api/v1/rename/project/',{
                project:id,
                new_name:new_name
            },function(data){
                if(data.status == 'cannot-rename'){
                    self.showMessage('Невозможно переименовать проект в данной папке!','alert-danger');
                }else{
                    var projects = self.body.find('.cont-main');
                    projects.find('#'+id).find('.project-name').text(new_name);
                }
            });
        });
    };
    GUI.prototype.renameFolder=function(){
        var self = this;
        this.askUser('Введите название папки',true,function(folder_name){
            var dir = self.body.find('#directs').find('.main_dir_active');
            if(!dir){
                self.showMessage('Выберите папку!','alert-danger');
            }
            if(folder_name.length<3){
                self.showMessage('Название папки должно быть не мение трёх символов','alert-danger');
            }else{
                $.post('/api/v1/rename/folder/',{'action':'rename','folder_name':folder_name,'folder_id':dir.attr('id')},function(data){
                    if(data.status=='cannot-rename'){
                        self.showMessage('Данную папку нельзя переименовывать','alert-danger');
                    }else if(data.status == 'ok'){
                        dir.text(folder_name);
                    }
                },'json');
            }

        });
    };
    GUI.prototype.createFolder=function(){
        var self = this;
        this.askUser('Введите название папки',true,function(folder_name){

            if (folder_name.length >= 3) {
                $.post('/api/v1/create/folder/', {'action': 'create', 'folder_name': folder_name}, function (data) {
                    if (data.status == 'created') {
                        var dirs = self.body.find('#directs');
                        var dir_element = $('<div class="main_dir"><a id=' + data.id + ' class="main-dir-folder"><span>' + folder_name + '</span></a></div>');
                        dirs.append(dir_element);
                        dir_element.bind('click', function () {
                            var el = $(this);
                            dirs.find('.main_dir_active').removeClass('main_dir_active');
                            el.addClass('main_dir_active');
                            self.loadProjects(el.attr('id'), self.body.find('.cont-main'));
                        });
                    } else if (data.status == 'exists') {
                        self.showMessage('Папка с таким именем уже существует!', 'alert-danger');
                    }
                }, 'json');
            } else {
                self.showMessage('Название папки должно быть не мение трёх символов', 'alert-danger');
            }

        });
    };
    GUI.prototype.deleteFolder=function(){
        var self=this
            ,dir = self.body.find('#directs').find('.main_dir_active');

        if(!dir.attr('id')){
            self.showMessage('Выберите папку!','alert-danger');
        }else{
            self.showQuestion('Вы действительно хотите удалить папку '+dir.text()+' и все проекты в ней ?', 'Удаление папки',function(){
                $.post('/api/v1/delete/folder/',{'action':'delete','folder_id':dir.attr('id')},function(data){
                    if(data.status == 'cannot-delete'){
                        self.showMessage('Данную папку нельзя удалять!','alert-danger');
                    }else if(data.status == 'ok'){
                        dir.remove();
                        self.body.find('.cont-main').children().remove();
                    }
                });
            });
        }

    };
    GUI.prototype.loadFolders=function(dirs,projects){
        var self = this;
        $.getJSON('/api/v1/get/folders/by/user/'+this.user_id+'/',function(data){
            dirs.children().remove();
            for(var i=0; i <= data.length; i++){
                var dir = data[i];
                if(dir){
                    var dir_element = $('<div data-type="'+dir.type+'" id="'+dir.id+'" class="main_dir"><a  class="main-dir-folder" data-toggle="tooltip" data-placement="top"  title='+dir.human_name+'><span >'+dir.human_name+'</span></a></div>')
                    if(i==0){
                        dir_element.addClass('main_dir_active')
                        self.loadProjects(dir_element.attr('id'),projects)
                    }
                    dir_element.bind('click',function(){
                        var el = $(this);
                        dirs.find('.main_dir_active').removeClass('main_dir_active')
                        el.addClass('main_dir_active')
                        self.loadProjects(el.attr('id'),projects)
                    });
                    dirs.append(dir_element);
                    dir_element.droppable({
                        drop: function(event, ui) {
                            var id_project = ui.draggable.parent().attr('id')
                                ,id_project_folder = ui.draggable.parent().attr('data-folder')
                                ,el = $(this)
                                ,type = el.attr('data-type')
                                ,id_folder = el.attr('id')


                            if(type == '2'||id_folder == id_project_folder){
                                self.showMessage('Невозможно перенести проект в эту папку!','alert-danger');
                            }else{
                                self.moveProject(id_project, id_folder);
                            }
                        }});
                    dir_element.tooltip()
                }
            }
        });
    };
    GUI.prototype.loadProjects=function(id,projects){
        var self=this,
            type=undefined;
        $.getJSON('/api/v1/get/folder/'+id+'/',function(data){
            type = data.type;
            projects.find('.project').remove();
            for(var i=0;i<=data.projects.length;i++){
                var project = data.projects[i];
                if(project){

                    var server_name=project.server_name.length!=0&&type!=2?'http://'+self.user_name+'-'+project.server_name+'.ajetlp.com':'';
                    var project_element =$('<tr class="project" data-folder="'+id+'" id="'+project.id+'">' +
                    '<td class="project-name">'+project.project_name+'</td><td class="server-name"><a target="_blank" href='+server_name+'>'+server_name+'</a></td><td>'+project.last_edit.replace('T',' ').replace('Z',' ')+'</td>' +
                    '</tr>');
                    project_element.on('click',function(){
                        projects.find('.current').removeClass('current');
                        $(this).addClass('current');
                    });
                    projects.append(project_element);
                    var name=project_element.find('.project-name')
                    name.draggable({
                             cursor: "none",
                             helper: function( event ) {
                                return $('<div id="'+name.parent().attr('id')+'" ><img src="/static/img/icons/1423665882_document-new.png"/></div>');
                            }
                    });
                    project_element.tooltip();
                }

            }
        });

    };
    GUI.prototype.setAuthor=function(author){
      $('#author').text(author);
    };
    GUI.prototype.setCurrentProjec=function(data){
      this.currentProject={
          id:data.id,
          project_name:data.project_name,
          server_name:data.server_name,
          folder:data.folder
      } ;
      this.setProjectName(data.project_name)
    };
    GUI.prototype.openProject=function(id){
        var self=this;
        $.getJSON('api/v1/open/project/'+id+'/',function(data){
            self.clearField()
            self.setCurrentProjec(data)
            Geometry.fromJSON(data.json_structure);
        });
    };
    GUI.prototype.setProjectName=function(name){
         $('#name-project').text(name);
    };
    GUI.prototype.moveProject=function(project_id,folder_id){
        var self = this;
         $.ajax({
            type:'POST',
            enctype:'multipart/form-data',
            dataType:'json',
            url:'/api/v1/move/folder/project/',
            data:{
                'action':'move-project-folder',
                'project_id':project_id,
                'folder_id':folder_id
            }
        }).done(function(data){
            if(data.status == 'moved'){
                $('.cont-main').find('#'+project_id).remove();
            }else{
               self.showMessage('Не удалось перенести папку.','.alert-danger');
            }
        });
    };
    GUI.prototype.prosedButtonsActions=function(){
        var action = $(this).attr('data-action')
            ,self = GUI.getInstance()
            ,project = self.body.find('.cont-main').find('.current')
            ,project_id = project.attr('id');
        if(!project_id){
            self.showMessage('Вы должны выбрать проект!','alert-danger');
            return false;
        }
        switch(action){
            case 'edit':
               self.openProject(project_id);
               return false;
            case 'get-link':
                self.showLink();
                return false;
            case 'delete':
                self.deleteProjectsById([project_id],'delete-projects');
                return false;
            case 'rename':
                self.renameProject(project_id);
                return false;
            case 'delete-file':
                if(project.find('.server-name').text().length==0){
                    self.showMessage('Проект не опубликован!','alert-danger');
                }else{
                    self.deleteProjectsById([project_id],'delete-files');
                }
                return false;

        }

    };
    GUI.prototype.showOpenAndSaveDialog=function(save){

        var dialog=$('#Modal_open_save')
            ,self = this
            ,dirs = dialog.find('#directs')
            ,projects = dialog.find('.cont-main')
            ,input = dialog.find('#input_name')
            ,input_val = input.children();

        if(save){
            dialog.find('.good').text('Сохранить');
            input.show()
            input_val.val('')
        }else{
            dialog.find('.good').text('Открыть');
            input.hide()
        }
        dialog.fb('.btn-purple','click',self.prosedButtonsActions);

        self.loadFolders(dirs,projects);

        dialog.modal('show');

        dialog.fb('.desktop-search-ico','click',self.findProjectsByName);

        dialog.fb('.good','click',function(){
           if(save){
               if(input_val.val().length<3){
                   self.showMessage('Проект должен иметь название не мение 3 символов','alert-danger');
               }else{
                    self.saveProject({
                        'humanName':input_val.val(),
                        'folder':dialog.find('.main_dir_active').attr('id'),
                        'dialog':dialog
                    });
               }
           }else{
                self.openProject(projects.find('.current').attr('id'));
                dialog.modal('hide')
           }
        });
    };
    GUI.prototype.findProjectsByName=function(){
        var search_val = $('.desktop-search').val(),
        gui = GUI.getInstance(),
        projects = $('.cont-main').find('.project');
        if(search_val.length < 3 ){
            gui.showMessage('Введите больше трёх символов','alert-danger');
        }else{
            projects.map(function(i,el){
                el = $(el).find('.project-name');
                if(el.text().indexOf(search_val)==-1){
                    el.parent().remove();
                }

            });
        }
    };
    /**
     *  Will save active project
     */
    GUI.prototype.saveProject=function(params){

        //humanName,folder,exists,diskName
        var self=this,
        //Simple data
        data={
            name:params.humanName,
            json:Geometry.toJSON(),
            folder:params.folder
        },
        progress = $('#Modal_progress');

        //If we try to create file in hosting - send file name & html content
        if(params.server_name){
            data.server_name=params.server_name;
            data.html=self.generateHTML(Geometry.contentElements,false);
        }
        progress.modal('show');
        $.ajax({
            type:'POST',
            enctype:'multipart/form-data',
            dataType:'json',
            url:'/api/v1/save/project/',
            data:data
        }).done(function(data){
            
            progress.modal('hide');
            self.updateInfo();
            if(data.status=='error'){
                switch(data.detail){
                    case 'max-pages-pablished' :
                        self.showMessage('Вы сохранили максимальное количество воронок на своём тарифе','alert-danger');
                        break;
                }
            }else{
                if(params.dialog){
                    params.dialog.modal('hide');
                }
                if(params.callback){
                    params.callback();
                }

                self.showMessage('Сохранено');
                self.setCurrentProjec({id:data.id,project_name:params.humanName,server_name:params.server_name,folder:params.folder})

            }
        });

    };
    /**
     * Will delete project by id
     */
    GUI.prototype.deleteProjectsById=function(ids,action){
        var self=this;

        if(this.currentProject.id in ids){
            this.showMessage('Вы не можете удалить открытый проект');
        }else{
            var onOk =function(){
                    $.post('/api/v1/delete/projects/',{
                    projects:ids,
                    action:action
                },function(data){
                    var projects = self.body.find('.cont-main');
                    for(var i in data){
                        if(data[i]=='deleted'){
                            self.updateInfo();
                            switch(action){
                                case 'delete-projects':
                                    projects.find('#'+i).remove();
                                    break;
                                case 'delete-files':
                                    projects.find('#'+i).find('.server-name').text('');
                                    break;
                            }
                        }
                    }

                },'json')
            };
            self.showQuestion(action=='delete-projects'?'Вы действительно хотите удалить проект?':'Вы действительно хотите убрать проект из опубликованных?', 'Удаление проекта', onOk);
        }
    };
    /**
    *   Wilss save CURRENT PROJECT
    */
    GUI.prototype.saveCurrentProject=function(){
        var self=this;
        if(self.currentProject.project_name){
            this.showQuestion('Вы действительно хотите перезаписать проект '+self.currentProject.project_name+' ?', 'Сохранение проекта', function(){
                if(self.currentProject.id&&self.currentProject.project_name){
                    self.saveProject({
                        humanName:self.currentProject.project_name,
                        folder:self.currentProject.folder,
                        exists:true,
                        server_name:self.currentProject.server_name
                    });
                }
            });
        }else{
            self.askUser('Введите имя проекта',true)
        }
    };
    GUI.prototype.askUser=function(msg,input,callback){
        var dialog = $('#Modal_ask_user')
            ,input = dialog.find('#input_name');
        if(input){
            input.show();
            input.children().val('')
        }else{
           input.hide();
        }
        dialog.find('#question').text(msg);

        dialog.fb('.good','click',function(){
           dialog.modal('hide');
           if(callback){
               if(input){
                   callback(input.children().val());
               }else{
                    callback();
               }
          }

        });
        dialog.modal('show');
    };
    /*
     *Resize elements
     */
    GUI.prototype.resizer=function(el,e){
        var cords=el.getCords(),
        condition=null, skiped=false;
        if(typeof e.ctrlKey!='undefined'){
            condition=e.keyCode;
        }else{
            condition=e['condition'];
            e['ctrlKey']=true;
                
        }
        switch(condition){
            case 'center-top':
            case 38: //UP
                if(e.ctrlKey&&el.el.resizable){
                    var t=el.JQueryEL.position().top;
                    el.JQueryContent.css({
                        'height':(parseInt(el.JQueryContent.css('height'))+(e.altKey?-1:+1))+'px'
                    });
                    el.JQueryEL.css({
                        'height':(parseInt(el.JQueryEL.css('height'))+(e.altKey?-1:+1))+'px',
                        'top':t+(e.altKey?+1:-1)
                    });

                }else{
                    --cords.y;
                }
                break;
            case 'center-bottom':
            case 40: //DOWN
                if(e.ctrlKey&&el.el.resizable){
                    el.JQueryContent.css('height',(parseInt(el.JQueryContent.css('height'))+(e.altKey?-1:+1))+'px');
                    el.JQueryEL.css('height',(parseInt(el.JQueryEL.css('height'))+(e.altKey?-1:+1))+'px');
                }else{
                    ++cords.y;
                }
                break;
            case 'middle-right':
            case 39: //LEFT
                if(e.ctrlKey&&el.el.resizable){
                    el.JQueryContent.css('width',(parseInt(el.JQueryContent.css('width'))+(e.altKey?-1:+1))+'px');
                    el.JQueryEL.css('width',(parseInt(el.JQueryEL.css('width'))+(e.altKey?-1:+1))+'px');
                }else{
                    ++cords.x;
                }
                break;
            case 'middle-left': 
            case 37: //right
                if(e.ctrlKey&&el.el.resizable){
                    var l=el.JQueryEL.position().left;
                    el.JQueryContent.css('width',(parseInt(el.JQueryContent.css('width'))+(e.altKey?-1:+1))+'px');
                    el.JQueryEL.css({
                        'width':(parseInt(el.JQueryEL.css('width'))+(e.altKey?-1:+1))+'px',
                        'left':l+(e.altKey?+1:-1)
                    });
                }else{
                    --cords.x;
                }
                break;
            default:
                skiped = true;
                break;
               
        }

        if(e.ctrlKey){
        //WTF where              
        }else{
            if(Geometry.isElementsContains(el.JQueryEL,this.panel_content))
                el.move(cords.x,cords.y); 
        }
        if(!skiped){
            return false;
        }
        
    };
    /**
     *  Click handler
     */
    GUI.prototype.onMouseClick=function(e){

        var self=this;
        
        if($(e.target).hasClass('elem_li_active'))
            return false;
        self.body.unbind('keydown');
        self.body.unbind('keypress');


        if($('.menu-edit').is(':visible')){
            $('.menu-edit').hide(200);
        }
         
        var el=$(e.target);
        el.parents('div').each(function(){
            var id=$(this).attr('id');
            if(!isNaN(parseInt(id)))
                el=Geometry.contentElements[id];
        });
        if(!el)
            return;//??
        if(!el.JQueryEL)
            el=Geometry.contentElements[el.attr('id')];
        if(el==undefined||el.getCords==undefined)
            return;
   
        self.body.bind('keydown',function(e){
            return self.resizer(el, e);
  
        });
    };
    /***
     *Informate user
     */
    
    GUI.prototype.showMessage=function(msg,alert){
        var form_info=$('#Modal_message');
        if(!alert){
            alert='alert-info';
        }
        var alert_cls=form_info.find('.alert');

        alert_cls.removeClass('alert-info');
        alert_cls.removeClass('alert-warning');
        alert_cls.removeClass('alert-danger');

        alert_cls.addClass(alert)

        form_info.find('#message').text(msg)
        form_info.modal('show');
        return form_info;
    };
    
    GUI.prototype.clearField=function(askUser){
        var self=this;
        var clear=function(){
            for(var i in Geometry.contentElements){
                var el=Geometry.contentElements[i];
                el.JQueryEL.remove();
            }
            
            Geometry.contentElements={};
            
            self.panel_content.css('background','');
            self.setProjectName('Безымянный проект')
            self.panel_content.attr('data-keywords','');
            self.panel_content.attr('data-trap','');
            self.panel_content.attr('data-trap-active','');
            self.currentProject={};
        }
        if(askUser){
            this.showQuestion('Вы действительно хотите очистить рабочую область? Все не сохранённые данные будут потеряны.', 'Создание нового проекта',function(){
                clear();
            });
        }else{
            clear();
        }
    }
    GUI.prototype.generateHTML=function(elements,excape){
        //FUCK!
        var html='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><title>'
        +(this.panel_content.attr('data-title')==undefined?$('.page_name').text():this.panel_content.attr('data-title'))+
        '</title>'+
        '<link type="image/x-icon" rel="shortcut icon" href="'+(this.panel_content.attr('data-favicon')==undefined?'http://constructor.mpbat.xyz/static/img/img_logo.png':this.panel_content.attr('data-favicon'))+'">'+
        //////////////////////////////////////////////////////////////////////
        '<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">'+
        '<script src="http://code.jquery.com/jquery-1.10.2.js"></script>'+
        '<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>'+
        //////////////////////////////////////////////////////////////////////
        '<script src="http://constructor.mpbat.xyz/static/js/jwplayer.js" type="text/javascript"></script>'+
        '<link type="text/css" rel="stylesheet" media="screen" href="http://constructor.mpbat.xyz/static/css/capture-sys.css?1">'+
        '<script src="http://constructor.mpbat.xyz/static/js/base.js" type="text/javascript"></script>'+
        //////////////////////////////////////////////////////////////////////////////
        '<meta content="'+this.panel_content.attr('data-keywords')+'" name="keywords">'+
        '<meta property="og:image" content="/site.png" />'+
        '</head>'+
        '<body style= \'margin:0;height:'+(parseInt(this.panel_content.css('height'))+12+parseInt(this.panel_content.css('border-bottom-width'))*2)+'px'+';padding:0;background-color:'+
        this.panel_content.css('background-color')+';'+
        'background-attachment:fixed;'+
        'background-size: 100% 100%;'+
        'background-image:'+this.panel_content.css('background-image')+
        ';\'>'+
        
        '<div style="margin: 6px 0 6px -'+(parseInt(this.panel_content.css('width'))/2)+'px;'+
        'position:relative;'+
        'left:50%;'+
        'width:'+this.panel_content.css('width')+';'+
        'height:'+this.panel_content.css('height')+';'+
        'border:'+this.panel_content.css('border-bottom-style')+' '+this.panel_content.css('border-bottom-width')+' '+this.panel_content.css('border-bottom-color')+
        ';" >'+
        '<div style="display:none;" id="key">'+this.user_id+'</div>'+
        '<div style="display:none;" id="page">'+this.currentProject.project_name+'</div>';
        var _p=this.panel_content;
        if(_p.attr('data-trap-active')=='true'){
            html+='<div id="trap" style="top:0;left:0;position:absolute;width: 0%; height: 0%; border: 0px;"><iframe style="overflow:scroll;width: 100%; height: 100%; border: 0px;" src="'+_p.attr('data-trap')+'"></iframe></div>'
        }
        //-------------------------------------------------------
        //          Create clear-HTML from content el
        //-------------------------------------------------------
        var unpack=function(parent_cloned,pos){
            var el=parent_cloned.children('.wrap-el').children();
            el.css({
                'position':'absolute',
                'left':pos.left,
                'top':pos.top,
                'width':pos.width,
                'heigth':pos.heigth,
                'z-index':pos.zIndex
            });
            //For stupid chrome
            //Chrome cannot understand $.css('background-size : 100% 100%'), and will do 'background-size: 100%' insted.
            el.attr('style',el.attr('style').replace('background-size: 100%;','background-size : 100% 100%;'));
            return parent_cloned.children('.wrap-el').html();
            
        };
        
        for(var i in elements){
            var el=elements[i];
            var parent_clear=el.JQueryContent.parent().parent();
            var parent_cloned=parent_clear.clone();
            var pos={
                left:(parseInt(el.JQueryEL.css('left'))+15)+'px',
                top:(parseInt(el.JQueryEL.css('top'))+15)+'px',
                width:el.el.resizable?(
                    !parent_clear[0].style.width?parent_clear.css('width'):parent_clear[0].style.width
                    ):'auto',
                height:el.el.resizable?parent_clear.css('height'):'auto',
                zIndex:parseInt(parent_clear.css('z-index'))

            }

            if(el.el.unpack){
                html+=Prototypes.Unpacks[el.el.unpack].call(el,pos);
            }else{
                html+=unpack(parent_cloned,pos);
            }    
        }

        html+='</div></body></html>';
        if(excape)
            html=html.replace(/</g,'&lt;').replace(/>/g,'&gt;');
        return html;
    }
    /**
     * Ask user
     */
    GUI.prototype.showQuestion=function(msg,title,onOk,onCancel){
        var form = $('#Modal_question');
        self.dialogOpened=true;
        form.find('#question').text(msg);
        form.find('#myModalLabel').text(title)

        form.fb('.good','click',function(){
            if(onOk)
                onOk();
            form.modal('hide')
            self.dialogOpened=false;
        });

        form.fb('.cancel','click',function(){
            if(onCancel)
                onCancel();
            self.dialogOpened=false;
            form.modal('hide')
        });
        form.modal('show');
        
    };
    return GUI;
};
