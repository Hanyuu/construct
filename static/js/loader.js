'use strict'
var p = console.log.bind(console);
//chrome://memory-internals/
$(document).ready(function(){


    var gui=new GUI()
        ,body=gui.body
        ,world=new World(gui)
        ,nav_menu=$('.instruments.nav-menu')
        ,additional=$('.additional-instruments')
        ,grid=$('.grid')
        ,pull_right=$('.instruments.pull-right')
        ,pull_left=$('.instruments.pull-left')
        ,navbar=$('.header.navbar-fixed-top')
        ,_window = $(window);


    _window.scroll(function (){
        var c=$('#fix');
        if(document.body.scrollLeft>0){
            c.css({'position':'inherit'});
            var block = $('.line_height');
                block.addClass('line_height2');
            var block3 = $('.grid');
                block3.addClass('grid1');
        }else{
            c.css({'position':'fixed','margin':0,'margin-top': '0px','margin-left': '18px'});
            var block1 = $('.line_height');
                block1.removeClass('line_height2');
            var block4 = $('.grid');
                block4.removeClass('grid1');
            }
    });

    new ElPanel('el-youtube','Элемент видео. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-text','Элемент текст. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-img','Элемент картинка. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-visit-counter','Элемент счётчик посещений. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-div','Элемент контейнер. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-href','Элемент ссылка. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-html','Элемент для вставки HTML кода. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-audio','Элемент для вставки звука. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-social-buttons','Элемент для вставки кнопок соц.сетей. Для активации элемента перетащите его на рабочую область.');
    new ElPanel('el-anchor','Элемент для якоря. Для активации элемента перетащите его на рабочую область.');

    $('[data-toggle="tooltip"]').tooltip();




    var replace_instraments=function(){

        if(_window.width()<=1610){
           body.css({
               'margin-top': '30px'
           });
        }else{
            body.css({
               'margin-top': '0px'
           });
        }

    };
    replace_instraments();
    _window.resize(function(e){
        replace_instraments();
    });
    //View HTML code or page preview
    $('.view, .html, .html_view').bind('click',function(e){
        var html,
            recipe =  window.open();
        if(e.ctrlKey){
            html=Geometry.toJSON().replace(/</g,'&lt;').replace(/>/g,'&gt;');
        }else{
            html=gui.generateHTML(Geometry.contentElements,
                ($(this).hasClass('html')||
                    $(this).hasClass('html_view'))
                );     
            
            if($(this).hasClass('html')||$(this).hasClass('html_view'))
                html=html.replace(/&gt;/gi,'&gt;<br/>');
            
        }

        $(recipe.document).ready(function(){
            recipe.document.write(html)
        });

        return false;
    });

    //Save page
    $('.save, .save-as').bind('click',function(){
        if($('#name-project').text().indexOf('Безымянный проект')!=-1){
            gui.showOpenAndSaveDialog(true);
        }else{
            gui.saveCurrentProject();
        }
        
    });
    //Clear content field
    $('#clear, #clear_two').bind('click',function(){
        gui.clearField(true);  
    });
    //Click handler
    body.bind('click',function(e){
        gui.onMouseClick(e);
    });
    //For edit CSS props of conten field;
    $('#edit, #edit_two').bind('click',function(){
         Prototypes.MenuFunctions.areaEdit(true);
    });

    //new template
    $('#create-new').bind('click',function(){
        gui.clearField(true);
    });

    //For stupid users prevent unload window
    _window.bind('beforeunload',function(){
        if(location.host.toString().indexOf('127.0.0.1')==-1
            &&
            location.host.toString().indexOf('localhost')==-1
            &&
            location.host.toString().indexOf('.frt')==-1)
            return 'Все не сохранённые данные будут потеряны.';
    });

    //CTRL+Z <-
    $('.back').bind('click',function(){
        world.rollBack();
    });
    //CTRL+Z ->
    $('.next').bind('click',function(){
        world.rollForward();
    });
    //Show open diaolog
    $('.open').bind('click',function(){
        gui.showOpenAndSaveDialog(false);
    });

    //Publish project
    $('.public').bind('click',function(){
        gui.publishProject();
    });
    //Show link
    $('.link,.http').bind('click',function(){
        gui.showLink(); 
    });
    //Clear field
    $('.new').bind('click',function(){
        gui.clearField(true);
    });
    //Show panel
    $('.panel_hide').bind('click',function(){
        $(this).toggleClass('panel_show') 
        $(this).parent().toggleClass('element_hide'); 
    });

    $('.create-open').bind('click',function(){
        gui.createFolder();
    });
    $('.rename-open').bind('click',function(){
        gui.renameFolder();
    });
    $('.delete-open').bind('click',function(){
        gui.deleteFolder();
    });

    
});

