
/***
 * This script not capture part!
 * This script include in generated page.
 */

$(document).ready(function(){
    var counter=$('#visit-counter')
		,socials=$('#social_network')
    if (socials.attr('id')=='social_network')
        socials.html(socials.html().replace(/%1/gi,document.location.href).replace(/%2/gi,document.title));
    //We has counter, update it!

    if(counter.attr('id')=='visit-counter'){
        
        $.getJSON("http://a-jetlanding.com/api/v1/update/visit/count/?callback=?",{
            user_id:$('#key').text(),
            page:$('#page').text()
        },function(data){
            counter.find('span').text(data['count']);
        });
    }
    if ($('#ga-code').attr('id')!=undefined){
        (function(i,s,o,g,r,a,m){
            i['GoogleAnalyticsObject']=r;
            i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)
            },i[r].l=1*new Date();
            a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];
            a.async=1;
            a.src=g;
            m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', $('#ga-code').attr('data-ga-code'), document.location.origin);
        ga('send', 'pageview');
    }

    if($('#trap').attr('id')=='trap'){
        $(window).unbind('beforeunload').bind('beforeunload',function(e){
            $(document.body).children('div').find('*').each(function(){
                var el=$(this);
                if((el.attr('id')==undefined&&el.parent().attr('id')==undefined)||el.attr('id')=='social_network'){
                    el.remove();
                }
               
            });
            $(document.body).find('object').each(function(){
				$(this).remove();
			});
            e.preventDefault?e.preventDefault():e.returnValue = false;
            $(document.body).css('height','').children('div').attr('style','');
            counter.remove();
            $('#trap').css({
                width:'100%',
                height:'100%',
                'z-index':99999
            });
            return '';
        });
        
    }
});