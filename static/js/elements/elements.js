'use strict'
var element=undefined;
/**
 *  @author Hanyuu
 *  EL on tools panel.
 *  @param type - type of element. </br>
 *  Handler for given type, must be defened in ElAppended constructor.</br>
 *
 */
var ElPanel=new function(){

    function ElPanel(type,toolTip){
        var self=this;
        this.type=type;
        this.drop=$('.drop');
        this.clearHTML='<li>'+
                   '<div class="element-pic '+this.type+'"data-toggle="tooltip" data-placement="left" title="'+toolTip+'"></div>'+
                '</li>'
        ;
        this.JQueryEL=$(this.clearHTML);

        this.JQueryEL.draggable(
            {
                cursor: 'move',
                helper: 'clone',
                start:function(){
                    element=self;
                }
            }

        );
        this.JQueryEL.appendTo(this.drop);
        $('.panel-content').droppable({
            drop: function(e) {
                if(element!=undefined){
                    new ElAppended(Prototypes.ContentElements[element.type],e);
                    element=undefined;
                }
            }
        });


    }

    return ElPanel;
};
/**
 *  Element in content panel.
 *  @param prototype : element from Prototypes.ContentElements<br/>
 *  @param e : mouse event<br/>
 *  @param fromTmpl : is loaded from template?
 */
var ElAppended=new function(){
    function ElAppended(prototype,e,fromTmpl){
        var self=this;
        this.menu=$('<div class="menu-edit"></div>');
        this.el=prototype;
        this.id=prototype.id?prototype.id:new Date().getTime();
        this.panel_content = $('.panel-content');
        if(this.el){
            /**
            * Realy displayed HTML object. (youtube, img, etc)
            */
            this.JQueryContent=$(this.el.html);
            
            if(this.el.onlyOne){
                var id=this.JQueryContent.attr('id');
                if(this.panel_content.find('#'+id).attr('id'))
                    return;
            }
            /**
            * Wrapper on realy displayed object
            */
            this.JQueryEL=$('<div id="'+this.id+'"><div class="wrap-el" id="wrap-'+this.id+'"></div></div>');
            this.JQueryContent.appendTo(this.JQueryEL.find('#wrap-'+this.id));
            this.JQueryEL.appendTo(this.panel_content);
            
            var panelCords={
                left:0,
                top:0
            };
            if(!fromTmpl||(fromTmpl&&!this.el.fromNew))
                panelCords=this.panel_content.offset();
            this.JQueryEL.css({
                position:'absolute',
                left:(e.pageX-panelCords.left)+'px',
                top:(e.pageY-panelCords.top)+'px',
                padding:'15px',
                'z-index':10
            }).css({
                border:'dotted 1px #656565'
            });

            /**
             *  Add actions to menu
             */
            this.el.actions.map(function(x){
                if(x){
                    var action=Prototypes.MenuFunctions[x.name];
                    if(!action)
                        throw 'Unknown action:'+x.name;
                    var actionLink=$('<span></span>');
                    actionLink.bind('click',function(){
                        self.menu.hide(0);
                        action.call(self);
                        return false;
                    });
                    actionLink.addClass(x.clazz);
                    actionLink.appendTo(self.menu);
                }
            });
            self.menu.appendTo(this.JQueryEL).hide();

            this.JQueryEL.bind('mousedown',function(e){
                self.mouseDownHandler(e);  
                return false;
            });



            this.JQueryEL.bind('mousemove',function(){
                $('.menu-edit').hide();
                self.showMenu();
            });
            

            if(!fromTmpl){
                EventEmitter.getInstance().emit('contentAdded', {
                    e:e,
                    el:this
                });
            }

            if(this.el.resizable){
                this.JQueryEL.resizable({
                    handles: 'all',
                    containment: '.panel-content',
                    aspectRatio:(this.el.aspectRatio?true:false),
                    resize: function(){
                        if(!self.JQueryContent.hasClass('el_text')){
                            self.JQueryContent.css('width',(parseInt(self.JQueryEL.css('width'))-30)+'px');
                            self.JQueryContent.css('height',(parseInt(self.JQueryEL.css('height'))-30)+'px');
                        }
                    },
                    start: function() {
                        EventEmitter.getInstance().emit('contentChanged');

                    }
                }).ruelResize();

            }
            this.JQueryEL.draggable(
                    {
                        containment: '.panel-content',
                        cursor: 'move',
                        stop:function(){

                        }
                    }
                );


        }else{
            throw 'Unknown Element type: '+prototype;
        }
    }
    /**
     *  Show edit menu
     */
    ElAppended.prototype.showMenu=function(){

        this.menu.css({
            'z-index':(parseInt(this.JQueryEL.css('z-index'))+1)+'px'
        });
        this.menu.show(0);

        return false;
    };
    /**
     * Will move element to last cords.
     */
    ElAppended.prototype.moveBack=function(){
        this.move(this.startCords.x,this.startCords.y);
    };
    /**
     *  Handler for "mouse down" event, user want to select element.
     */
    ElAppended.prototype.mouseDownHandler=function(){
        this.JQueryEL.fb('a','click',function(e){
            e.preventDefault();
        });
        EventEmitter.getInstance().emit('contentChanged');

        this.panel_content.find('.selected-el').removeClass('selected-el');
        this.JQueryEL.addClass('selected-el');

    };
    /**
     *  Change Z-index for el & all childs
     */
    /*ElAppended.prototype.changeZIndex=function(count){
        var index=parseInt(this.JQueryEL.css('z-index'))+(count);
        if(index<10)
            return;
        this.JQueryEL.css({
            'z-index':index
        });

    };*/
    /**
     *  Return cords {x,y}
     */
    ElAppended.prototype.getCords=function(){
        return {
            x:parseInt(this.JQueryEL.css('left')),
            y:parseInt(this.JQueryEL.css('top'))
        };  
    };

    /**
     *  Del element
     */
    ElAppended.prototype.deleteSelf=function(){

        EventEmitter.getInstance().emit('contentDeleted', this);
        EventEmitter.getInstance().emit('contentChanged');

        var anchor = this.JQueryContent.attr('data-name');
        if (anchor&&anchor.length > 0){
            delete GUI.getInstance().anchors[anchor];
        }
        this.JQueryEL.remove();
        this.menu.remove();
        
    };   
    return ElAppended;
};
/**
 * "Abstract"
 * @see elementsFunctions.js
 * @see elementsPrototypes.js
 */
var Prototypes=new function(){
    function Prototypes(){}  
    return Prototypes;
};