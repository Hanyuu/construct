'use strict'

//-----------------------------------------------------------------------------------
//                          FUNCTIONS FOR EDITING ELEMENTS
//-----------------------------------------------------------------------------------

/**
 * Functions in element "menu" (Delete & edit). Each element has own edit function.
 */
Prototypes.MenuFunctions={
    /**
     * Edit content field & box element.
     */
    areaEdit:function(mainPage){

        var  form = $('#Modal_area_edit')
            ,el = undefined;
        if(mainPage!=undefined){
            el=$('.panel-content');
        }else{
            el = this.JQueryEL;
        }

        form.find('#field-height').val(parseInt(el.css('height')));

        form.find('#field-width').val(parseInt(el.css('width')));

        form.modal('show');

        form.appendBorderEditor(el);
        form.appendColorPicker(el);
        
        form.find('#field-height').val(parseInt(el.css('height')));
        
        form.find('#field-width').val(parseInt(el.css('width')));
        if(el.css('background-image').indexOf('none')==-1)
            form.find('#background-image').val(el.css('background-image').replace('url("','').replace('")','').replace('url(','').replace(')',''));
        if(mainPage){
            form.find('#main_page').show();
            //noinspection JSJQueryEfficiency
            form.find('#keywords').val($('.panel-content').attr('data-keywords'));
            //noinspection JSJQueryEfficiency
            form.find('#trap').val($('.panel-content').attr('data-trap'));
            //noinspection JSJQueryEfficiency
            form.find('#favicon').val($('.panel-content').attr('data-favicon'));
            //noinspection JSJQueryEfficiency
            form.find('#title').val($('.panel-content').attr('data-title'));
            //noinspection JSJQueryEfficiency
            form.find('#ga-code').val($('.panel-content').attr('data-ga-code'));
            //noinspection JSJQueryEfficiency
            if($('.panel-content').attr('data-trap-active')=='true'){
                form.find('#trap-active').attr('checked','true');
            }
        }else{
            form.find('#main_page').hide();
        }


            form.fb('.good','click',function(){
                var height = form.find('#field-height').val()+'px'
                    ,weight = form.find('#field-width').val()+'px';
                if(parseInt(weight)>1000&&mainPage!=undefined){
                    GUI.getInstance().showMessage('Ширина не может быть больше 1000!','alert-danger');
                    return;
                }
                 EventEmitter.getInstance().emit('contentChanged');

                var type=$(form.find('.btn-group').children('.active')[0]).attr('data-border');


                el.css({
                    'background-color':(form.find('#bg-color-edit').first().val().trim()),
                    'height':form.find('#field-height').val()+'px',
                    'width':form.find('#field-width').val()+'px',
                    'border':type+' '+form.attr('data-border-size')+' '+form.attr('data-border-color'),
                    'background-image':form.find('#background-image').val().length>0?'url("'+form.find('#background-image').val().trim()+'")':'',
                    'background-position':'fixed',
                    'background-size':'100% 100%',
                    'background-repeat': 'no-repeat'
                });
                if(mainPage!=undefined){
                    var panel_content=$('.panel-content');
                    panel_content.attr('data-keywords',form.find('#keywords').val());
                    panel_content.attr('data-trap',form.find('#trap').val());
                    panel_content.attr('data-favicon',form.find('#favicon').val());
                    panel_content.attr('data-title',form.find('#title').val());
                    panel_content.attr('data-ga-code',form.find('#ga-code').val());
                    panel_content.attr('data-trap-active',form.find('#trap-active').is(':checked'));
                    var width_count =parseInt(weight)/100;
                    var height_count = parseInt(height)/100;
                    var width_count_str = ''
                        ,height_count_str = '';
                    if(width_count>1){
                        width_count=Math.ceil(width_count)
                    }
                    if(height_count>1){
                        height_count=Math.ceil(height_count)
                    }
                    $('.grid').css({
                        'height':form.find('#field-height').val()+'px',
                        'width':form.find('#field-width').val()+'px'
                    });
                    for(var i = 1;i<=width_count;i++){

                        width_count_str+='<span>'+i*100+'</span>';
                    }
                    $('.line_wight .body').html(width_count_str);
                    $('.line_wight').css({
                        'width':weight
                    });



                    for(var i =1;i<=height_count;i++){
                        height_count_str+='<span>'+i*100+'</span>';
                    }
                    $('.line_height .body').html(height_count_str);

                    $('.line_height').css({
                        'height':height
                    });
                }

            form.modal('hide');
        });
    },
    /**
     * Delete element
     */
    deleteEl:function(){
        var form = $('#Modal_question');
        var self = this;
        form.find('#question').text('Вы действительно хотите удалить элемент?');
        form.find('#myModalLabel').text('Удаление Элемента');

        form.fb('.good','click',function(){
            self.deleteSelf();
            form.modal('hide');
        });

        form.fb('.cancel','click',function(){
           form.modal('hide');
        });
        form.modal('show');
    },
    /**
     * Edit youtube
     */
    editYoutube:function(){
        var el=this.JQueryContent,
            youtubeRGX=/http:\/\/(?:www\.)?youtu(?:be\.com\/(?:watch\?v=|v\/)|\.be\/)([^&]+).*$/i,
            videoId=el.attr('src').split('/')[4],
            isAuto=false;
        
        var form=$('#Modal_youtube_edit');

        form.find('#video').val('http://www.youtube.com/watch?v='+videoId);
        if(el.attr('data-autoplay'))
            form.find('#autoplay').attr('checked','true');

        form.fb('#autoplay','click',function(){
            isAuto=$(this).is(':checked')
        });

        form.fb('.good','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var link=form.find('#video').val();
            if(isAuto)
                el.attr('data-autoplay','true');
            else
                el.removeAttr('data-autoplay');
            if(youtubeRGX.exec(link.trim())){
                el.attr('src','http://i.ytimg.com/vi/'+RegExp.$1+'/default.jpg');
                form.modal('hide');
                return false;
            }else if(/^http:\/\/storage\.a-jetbox\.org\/storage\/file\/get\/([0-9]+)\/{0,1}$/.test(link.trim())){
                el.attr('src','/static/img/elements/player.png');
                form.modal('hide');
                return false;
            }else{
                //TODO SHOW ERROR WHERE
            }
        });
        form.modal('show');

    },
    /**
     * Edit text element
     */
    editText:function(){
        
        var el=this.JQueryContent
            ,form=$('#Modal_text_edit')
            ,instance;
        

        form.find('textarea').val(el.html());


        form.fb('.good','click',function(){
           
            EventEmitter.getInstance().emit('contentChanged');
            el.html(instance.getData());
            el.css({
                width:'100%',
                height:'auto'
            });
            form.modal('hide');
        });
        form.modal('show');
        if(CKEDITOR.instances['mail_text'] == undefined){
            CKEDITOR.replace('text-editor');
            instance=CKEDITOR.instances['mail_text'];
            CKEDITOR.config.width = 740;
        }else{
            instance=CKEDITOR.instances['mail_text'];
        }

    },
    /**
     * Edit link element
     */
    editLink:function(){
        var el=this.JQueryContent
                ,form=$('#Modal_link_edit')
                ,link=form.find('#link')
                ,gui=GUI.getInstance()
                ,css={
                    font_style:'',
                    font_weight:''
                };

        ///////////////////// STATIC


        link.attr('href',el.attr('href')).text(el.text()).css({
            'color':el.css('color'),
            'backgroud-color':el.css('background-color'),
            'border':el.css('border-bottom-style')+' '+el.css('border-bottom-width')+' '+el.css('border-bottom-color'),
            'font-weight':el.css('font-weight'),
            'font-style':el.css('font-style'),
            'text-decoration':el.css('text-decoration')
        });

        form.find('#select-link-anchor').children().attr('selected',false);

        if(el.attr('href').indexOf('#') == 0){
            form.find('#select-link-anchor').find('#anchor-select').attr('selected',true);
            form.find('#link-edit').hide();
            form.find('#anchor-edit').children().remove();
            var href=el.attr('href').replace('#','');
            for(var anchor in gui.anchors){
                if (anchor==href){

                    $('<option selected="true">'+anchor+'</option>').appendTo(form.find('#anchor-edit'));
                }else{

                    $('<option>'+anchor+'</option>').appendTo(form.find('#anchor-edit'));
                }

            }
            form.find('#anchor-edit').show();
        }else{
            form.find('#select-link-anchor').find('#link-select').attr('selected',true);
            form.find('#link-edit').show();
            form.find('#anchor-edit').hide();
        }

        if(el.css('text-decoration')=='underline'){
            form.find('#decoration').attr('checked','true');
        }


        if(parseInt(el.css('font-weight'))==700||el.css('font-weight').toString().indexOf('bold')>-1){
            form.find('#text-bold').addClass('active');
        }

        if(el.css('font-style').indexOf('italic')>-1){
            form.find('#text-italic').addClass('active');
        }

        //////////////// DYNAMIC


        form.attr('data-border-size',el.css('border-bottom-width'));

        form.appendBorderEditor(el,link);
        form.appendColorPicker(el,link);
        
        //Link text  
        form.find('#link-text-edit').val(el.text()).bind('keyup',function(){
            link.text($(this).val());
        });

        //Link href
        form.find('#link-edit').val(el.attr('href')).bind('keyup',function(){
            link.attr('href',$(this).val());
        });

        //Link text size    
        form.find('#text-height-edit').val(parseInt(el.css('font-size')));

        //Anchor or Link
        form.fb('#select-link-anchor','change',function(){
            var id = $(this).find(":selected").attr('id');
            switch(id){
                case 'link-select':
                    form.find('#link-edit').show();
                    form.find('#anchor-edit').hide();
                    break;
                case 'anchor-select':
                    form.find('#link-edit').hide();
                    form.find('#anchor-edit').children().remove();
                    for(var anchor in gui.anchors){
                        $('<option>'+anchor+'</option>').appendTo(form.find('#anchor-edit'));
                    }
                    form.find('#anchor-edit').show();
                    break;
            }
        });
        if(el.attr('target')=='_blank')
            form.find('#blank').attr('checked','true');
        
        form.fb('#blank','click',function(){
            if($(this).is(':checked'))
                link.attr('target','_blank') ;
            else
                link.attr('target','') 
        });

        form.fb('#decoration','click',function(){
            if($(this).is(':checked'))
                link.css('text-decoration','underline');
            else
                link.css('text-decoration','none');
        });

        
        form.fb('#text-height-edit','keyup',function(){
            link.css('font-size',$(this).val()+'px');
        });
        
        form.fb('#row-down','click',function(){
            var val=parseInt(form.find('#text-height-edit').val())-1;
            form.find('#text-height-edit').val(val);
            link.css('font-size',val+'px');
        });

        form.fb('#row-up','click',function(){
            var val=parseInt(form.find('#text-height-edit').val())+1;

            form.find('#text-height-edit').val(val);
            link.css('font-size',val+'px');
        });
        
        form.fb('#text-bold','click',function(){
            var el = $(this);
            if(css.font_weight==700||css.font_weight.toString().indexOf('bold')>-1){
                css.font_weight=400;
                el.removeClass('active')
            }else{
                css.font_weight=700;
                el.addClass('active')
            }
            link.css('font-weight',css.font_weight);
        });
        
        form.fb('#text-italic','click',function(){
            var el = $(this);
            if(css.font_style.indexOf('italic')>-1){
                css.font_style='normal';
                el.removeClass('active');
            }else{
                css.font_style='italic';
                el.addClass('active');
            }
            link.css('font-style',css.font_style);
            
        });
        
        form.fb('.good','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var text = form.find('#link-text-edit').val();
            var href = form.find('#link-edit').val();
            var anchor = form.find('#anchor-edit').find(':selected').text();
            if(text.length == 0 ){
                gui.showMessage('Введите текст ссылки!','alert-danger');
                return false;
            }

            el.text(text);
            if(form.find('#anchor-edit').is(':visible')){
                el.attr('href','#'+anchor);
            }else{
               el.attr('href',href);
            }

            var type=form.find('.btn-group').children('.active').attr('data-border');
            el.css({
                'background-color':form.find('#bg-color-edit').val(),
                'color':form.find('#text-color-edit').val(),
                'font-size':form.find('#text-height-edit').val()+'px',
                'font-style':css.font_style,
                'font-weight':css.font_weight,
                'text-decoration':link.css('text-decoration'),
                'border':type+' '+form.attr('data-border-size')+' '+form.attr('data-border-color')
            });
            
            el.attr('target',link.attr('target'));
            form.modal('hide')
        });
        form.modal('show')
    },
    /**
     * Edit image element
     */
    editImage:function(){
        var el=this.JQueryContent
            ,self=this
            ,form=$('#Modal_img_edit');
        
        form.find('#img-src-edit').val(el.attr('src'));
        form.find('#link-href').val(el.attr('img-href')==undefined?'':el.attr('img-href'));

        if(el.attr('link-active')=='true')
            form.find('#is-link').attr('checked','true');
        form.fb('.good','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var val=form.find('#img-src-edit').val();
            
            if(form.find('#is-link').is(':checked'))
                el.attr('link-active','true');
            else
                el.removeAttr('link-active');
            el.attr('img-href',form.find('#link-href').val());
            el.attr('src',val);
            el.css({
                'width':'auto',
                'height':'auto'
            });
            self.JQueryEL.css({
                'width':'auto',
                'height':'auto'
            });
            
            form.modal('hide');
        //}
        
        });
        form.modal('show');
    },
    editHtml:function(){
        var el=this.JQueryContent
            ,form=$('#Modal_html_edit');
        
        form.find('textarea').val(el.html());
        
        form.fb('.good','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            var html=form.find('textarea').val();
            /**
             * For [FLASH] bb code
             */
            if(/\[flash\=([0-9,0-9,A-z\.\/\:]+)/.test(html)){
                var code=RegExp.$1.split(',');
                
                var img='<img src="/static/img/elements/flash.png" style="width:'
                +code[0]+'px;height:'
                +code[1]+'px" data-flash="'
                +code[2].substring(0,code[2].length-1)
                +'"/>';
                el.html(img);
                el.parent().parent().css({
                    width:code[0]+'px',
                    height:code[1]+'px'
                });
            }else{
                /**
                 * For other code
                 */
                if(html.indexOf('-flash')!=-1){

                    html=html.replace('</object>','<param name="wmode" value="opaque" /></object>');

                }
                var _el=$('<div>'+html+'</div>');
                
                el.html(_el.html());
                el.css({
                    width:'auto',
                    height:'auto'
                }).parent().parent()
                .css({
                    width:'auto',
                    height:'auto'
                });
            }
            
            form.modal('hide');
        });
        
        form.modal('show');
    },
    /**
     * Edit audio element
     */
    editAudio:function(){
        
        var el=this.JQueryEL
            ,self=this
            ,form=$('#Modal_audio_edit')
            ,player_type;
        form.find('.text').val(el.children().children().attr('data-audio'));
        
        
        if(el.children().children().attr('data-onstart')=='true')
            form.find('#onstart').attr('checked','true');
        form.find('#player-type').val(el.attr('data-decoration-id')?el.attr('data-decoration-id'):1);
        form.find('#player-preview')
        .attr('src',form.find('#player-preview')
            .attr('src')
            .replace(/audio-view_s[0-9].jpg/,'audio-view_s'+(el.attr('data-decoration-id')?el.attr('data-decoration-id'):1)+'.jpg'));
        player_type=parseInt(form.find('#player-type').val());
        form.fb('#player-type','change',function(){

            var src=$('#player-preview').attr('src');
            player_type=$(this).val();


            $('#player-preview').attr('src',src.replace(/audio-view_s[0-9].jpg/,'audio-view_s'+player_type+'.jpg'));
            
        });
        form.fb('.good','click',function(){
            EventEmitter.getInstance().emit('contentChanged');
            el.children().children().attr('data-audio',form.find('.text').val());
            el.children().children().attr('data-onstart',form.find('#onstart').is(':checked'));
            
            switch(parseInt(player_type)){
                case 1:
                    self.JQueryContent.attr('data-decoration-id',1);
                    self.JQueryContent.attr('data-decoration','');
                    break;
                case 2:
                    self.JQueryContent.attr('data-decoration','bgcolor1=c9c8c8&amp;bgcolor2=717171&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=6B6966&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=6B6966" ');
                    self.JQueryContent.attr('data-decoration-id',2);
                    break;
                case 3:
                    self.JQueryContent.attr('data-decoration','bgcolor1=D4DE4E&amp;bgcolor2=63A45F&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=66A062&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=66A062"" ');
                    self.JQueryContent.attr('data-decoration-id',3);
                    break;
                case 4:
                    self.JQueryContent.attr('data-decoration','bgcolor1=008CD0&amp;bgcolor2=5B7BBB&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=3B5172&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=3B5172"" ');
                    self.JQueryContent.attr('data-decoration-id',4);
                    break;
                case 5:
                    self.JQueryContent.attr('data-decoration','bgcolor1=D65988&amp;bgcolor2=81589f&amp;loadingcolor=ffffff&amp;buttoncolor=e2e2e2&amp;buttonovercolor=61447A&amp;slidercolor1=e2e2e2&amp;slidercolor2=979695&amp;sliderovercolor=61447A"" ');
                    self.JQueryContent.attr('data-decoration-id',5);
                    break;
            }
            var img=el.find('#audio').children();
            var src=img.attr('src');
            if(src.indexOf('/static/img/elements/audio-view.png')!=-1){
                img.attr('src','/static/img/elements/'+'audio-view_s'+(player_type?player_type:1)+'.jpg');
            }else{
                img.attr('src',img
                    .attr('src')
                    .replace(/audio-view_s[0-9].jpg/,'audio-view_s'+(player_type?player_type:1)+'.jpg'));
            }
            form.modal('hide');
        });
        
        form.modal('show');
    },
    editAnchor:function(){
        var dialog = $('#Modal_anchor_edit')
            ,el = this.JQueryContent
            ,input = dialog.find('#edit-anchor')
            ,gui = GUI.getInstance();

        input.val(el.attr('data-name'));
        dialog.modal('show');
        dialog.fb('.good','click',function(){
            var value = input.val();

            if(value.length==0){
                gui.showMessage('Количество символов не может быть 0','alert-danger');
            }else if(/^[a-z0-9]{4,10}$/.test(value)){
                if(gui.anchors[value]){
                    gui.showMessage('Якорь с таким именем уже существует.','alert-danger');
                }else{
                    el.attr('data-name',value);
                    el.find('#text-anchor').text(value);
                    gui.anchors[value]='None';
                    dialog.modal('hide');
                }
            }else{
                gui.showMessage('Имя может состоять только из английских букв или цифр и иметь длнну от 4х до 10 символов.','alert-danger');
            }
        });

    }
};
