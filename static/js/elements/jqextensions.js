'use strict'
/**
*   @author Hanyuu
*   JQuery Extensions here.
*
*/


//-----------------------------------------------------------------------------------
//      JQuery EXTENSIONS
//-----------------------------------------------------------------------------------

$.fn.extend({
    ruelResize:function(){
        var el=$(this);
        el.addClass('resize');
        $('<span class="top-left"></span>'+
            '<span class="top-right"></span>'+
            '<span class="bottom-right"></span>'+
            '<span class="bottom-left"></span>'+
            '<span class="middle-left"></span>'+
            '<span class="middle-right"></span>'+
            '<span class="center-top"></span>'+
            '<span class="center-bottom"></span>')
        .appendTo(el);
        return this;
    } 
    
});
///-------------------------------------------------
///              BORDER EDITOR
///-------------------------------------------------
$.fn.extend({
    appendBorderEditor:function(el,additionalEl){
        /*
        Init section - begin
         */
        var form=$(this);
        if(additionalEl){
            additionalEl.css('border',el.css('border-bottom-style')+' '+parseInt(el.css('border-bottom-width')));
        }
       
        form.find('#btn-border-size').val(parseInt(el.css('border-bottom-width')));
        form.attr('data-border-size',el.css('border-bottom-width'));

        switch(el.css('border-bottom-style').trim()){
            case 'dashed':
                form.find('.frame-img').addClass('active');
                break;
            case 'inset':
                form.find('.frame-img1').addClass('active');
                break;
            case 'outset':
                form.find('.frame-img2').addClass('active');
                break;
            case 'double':
                form.find('.frame-img3').addClass('active');
                break;
            case 'none':
            case 'solid':
                form.find('.frame-img4').addClass('active');
                form.find('#btn-border-size').val(0);
                break;
        }
        /*
        Init section - end
         */
        /******************************************************/
        /*
        On change border style
         */
        form.fb('.btn-group','click',function(e){
            var e = $(e.target)
                ,el = $(this);
            el.find('.active').removeClass('active');
            e.addClass('active');
            if(!e.hasClass('frame-img4')){
                var bs=parseInt(form.find('#btn-border-size').val());
                if(bs==0){
                    form.find('#btn-border-size').val(4);
                    form.attr('data-border-size','4px');
                }else{
                    form.attr('data-border-size',bs+'px');
                }
            }

            if(additionalEl){
                additionalEl.css('border',e.attr('data-border')+' '+form.attr('data-border-size'));
            }

        });
        /*
        On change border size
        */
        form.fb('#btn-border-size','change',function(){
            var el = $(this);
            form.attr('data-border-size',(el.val()+'px'));
            if(additionalEl){
                additionalEl.css('border',form.find('.btn-group').children('.active').attr('data-border')+' '+form.attr('data-border-size'));
            }
        });
    } 
});

$.fn.extend({
    /**
     *  Find & bind
     */
    fb:function(selector,action,callback){
        this.find(selector).unbind(action).bind(action,function(e){
            callback.call(this,e);
        });
        return this;
    } 
});
/**
 * Return color in HEX format
 */
$.fn.extend({
    getHexColor:function(color){
        var val=this.css(color);
        if(!val)
            return;
        var toHex=function (N) {
            if (N==null) return "00";
            N=parseInt(N);
            if (N==0 || isNaN(N)) return "00";
            N=Math.max(0,N);
            N=Math.min(N,255);
            N=Math.round(N);
            return "0123456789ABCDEF".charAt((N-N%16)/16)
            + "0123456789ABCDEF".charAt(N%16);
        };
        if(val.indexOf('transparent')>-1){
            return 'transparent';
        }else if(val.indexOf('rgb(')>-1){
            var rgb=val.replace('rgb(','').replace(')','').replace(',','').split(' ');
            var r=rgb[0];
            var g=rgb[1];
            var b=rgb[2];
            return '#'+toHex(parseInt(r))+  
            toHex(parseInt(g))+
            toHex(parseInt(b));
        
        }else if(val.indexOf('#')>-1){
            return val;
        }
    } 
});

/**
 * Adding color picker to form
 */
$.fn.extend({
    appendColorPicker:function(el,additionalEl){
        var self=this
            ,add_color=function(){
                if(additionalEl){
                    var type=self.find('.btn-group').children('.active').attr('data-border');
                    additionalEl.css({
                        'background-color': self.find('#bg-color-edit').val(),
                        'color': self.find('#text-color-edit').val(),
                        'border':(type+' '+self.attr('data-border-size')+' '+self.find('#border-color-edit').val())
                    });
                }

            };
        self.find('#bg-color-edit').val(el.getHexColor('background-color'));
        self.find('#text-color-edit').val(el.getHexColor('color'));
        //TODO strange thing, my be fix?
        self.find('#border-color-edit').val(el.getHexColor('border-bottom-color'));

        self.find('#text-color').css({
            'background-color':el.css('color')
        });
        
        self.find('#bg-color').css({
            'background-color':el.css('background-color')
        });

        self.find('#border-color').css({
            'background-color':el.css('border-bottom-color')
        });
        
        self.attr('data-border-color',el.getHexColor('border-bottom-color'));

        /**
        * User can enter color in HEX by you self - TEXT
        */
        self.fb('#text-color-edit','keyup',function(){
            self.find('#text-color').css({
                  'background-color':$(this).val()
            });
            add_color();
        });
        /**
        * User can enter color in HEX by you self - FONT
        */
        self.fb('#bg-color-edit','keyup',function(){
            self.find('#bg-color').css({
                'background-color':$(this).val()
            });
            add_color();
        });
        /**
        * User can enter color in HEX by you self - BORDER
        */
        self.fb('#border-color-edit','keyup',function(){
            self.find('#border-color').css({
                'background-color':$(this).val()
            });
            add_color();
            self.attr('data-border-color',self.find('#border-color').getHexColor('background-color'));
        });

        /**
        * Set selected color by click
        */
        self.fb('.color, .no_color','click',function(){

            var el=$(this)
                ,parent=el.parent().parent().parent().parent().parent().parent()
                ,box=parent.children('div:first-child').children('div')
                ,input=parent.children('input');

            input.val(el.getHexColor('background-color'));

            if(input.attr('id')=='border-color-edit'){
                self.attr('data-border-color',el.getHexColor('background-color'));
            }
            
            box.css('background-color',el.css('background-color'));

            if(el.hasClass('no_color')){
                input.val('transparent');
                box.css('background-color','transparent');  
            }
            add_color();
        });

    }
});