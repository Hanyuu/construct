'use strict'
/**
 * @author Hanyuu
 * 
 * Prototypes of HTML elements.
 * html:raw html code of el
 * actions:will show in menu, {clazz:css class,name:point to funcion in Prototypes.MenuFunctions}
 * (optional) unpack: point to unpack function in Prototypes.Unpacks 
 * (optional) resizable: is resizable?
 * (optional) onlyOne:only one element of such type can added in panel-content
 */
Prototypes.ContentElements={
    /**
     * YouTube
     */
    'el-youtube':{
        html:'<img style="width:420px; height:315px;" src="http://i.ytimg.com/vi/1hBiHp2BwJY/default.jpg"></img>',
        actions:[{
            clazz:'edit',
            name:'editYoutube'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        unpack:'unpackYouTube',
        resizable:true,
        aspectRatio:true
    },
    /**
     * Text
     */
    'el-text':{
        html:'<div class="el_text" style="line-height:inherit; backgroud-color:transparent;float:left; width:351px;margin:0px;text-align:left;">Text</div>',
        actions:[{
            clazz:'edit',
            name:'editText'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true
    },
    /**
     * Img
     */
    'el-img':{
        html:'<img style="width:40px;height:40px;" src="http://ajetlanding.com/static/img/img_logo.png"/>',
        actions:[{
            clazz:'edit',
            name:'editImage'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true,
        aspectRatio:true,
        unpack:'unpackIMG'
    },
    /**
     * Box
     */
    'el-div':{
        html:'<div style="width:100px;height:100px;"></div>',
        actions:[{
            clazz:'edit',
            name:'areaEdit'
        },{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true
    },
    /*
     * Link
     */
    'el-href':{
        html:'<a style="text-decoration:underline" href="http://a-jetbox.com/">A-JETBOX</a>',
        actions:[{
            clazz:'edit',
            name:'editLink'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false
    },
    /**
     * Subscribe form
     */
    /*'el-form':{
        html:
        '<div style="float:left;"><style>'+
            'div.form{padding:10px;}div.form table{border-collapse: separate;} div.form,div.form div.button{float:left}div.form{font-family:Tahoma,"sans-serif"}div.form td.text{padding-right:10px;color:#343434}div.form td.text,div.form div.button a{font-size:14px}div.form input,div.form div.button{height:25px}div.form input{margin:0;padding:0 5px;border:solid 1px #7e7e7e;width:150px}div.form div.button{cursor:pointer;border-radius:4px;overflow:hidden;margin-top:4px;border-style:solid;border-width:1px}div.form div.button span{display:block;padding:3px 10px;height:100%;background:url("http://capture-sys.ruelsoft.org/img/form/button_bg.png") repeat-x 0 -2px}div.form table{border-spacing: 0 5px;}'+
       ' </style><div class="form">'+
                '<table class="table">'+
                    '<tbody>'+
                        '<tr id="FirstName">'+
                            '<td><input placeholder="Имя" type="text"/></td>'+
                        '</tr>'+
                        '<tr id="eMail">'+
                            '<td><input placeholder="E-Mail" type="text"/></td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+
                '<div style="border-color:#fc145b;background: #fc145b; color:#fff" class="button"><span>Подписаться</span></div>'+
        '</div></div>',
        actions:[{
            clazz:'edit',
            name:'editForm'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false
    },*/
    /**
     * Raw HTML input
     */
    'el-html':{
        html:'<div style="width:40px;height:40px"></div>',
        actions:[{
            clazz:'edit',
            name:'editHtml'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:true,
        unpack:'unpackHTML',
        aspectRatio:true
    },
    /**
     * Audio
     */
    'el-audio':{
        html:'<div id="audio" style="width:200px;height:20px" data-audio="http://a-jetlanding.com/static/audio/sound.mp3"><img src="/static/img/elements/audio-view.png" /></div>',
        actions:[{
            clazz:'edit',
            name:'editAudio'
        },
        {
            clazz:'delete',
            name:'deleteEl'
        }],
        unpack:'unpackAudio',
        resizable:false,
        onlyOne:true
    },
    'el-visit-counter':{
        html:'<div id="visit-counter" class="counter_block">'+
            '<div style="background: #30291F;  color: #ffffff; padding: 0px 2px;" >'+
                '<span>0</span>'+
            '</div>'+
        '</div>',
        actions:[{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false,
        onlyOne:true
    },
    /**
     * Social buttons
     */
    'el-social-buttons':{
        html:'<div id="social_network">'+
        '<span id="network">'+
                '<a target="_blank" title="Поделиться в Facebook" href="http://www.facebook.com/sharer.php?u=%1&amp;t=%2" style="background-position: -0px 0">'+
            '</a>'+
            '<a target="_blank" title="Поделиться В Контакте" href="http://vkontakte.ru/share.php?url=%1&title=%2" style="background-position: -32px 0" >'+
            '</a>'+
                '<a target="_blank" title="Добавить в Twitter" href="http://twitter.com/share?text=%2&amp;url=%1"  style="background-position: -64px 0">'+
            '</a>'+
                '<a target="_blank" title="Добавить в Одноклассники" href="http://www.odnoklassniki.ru/dk?st.cmd=addShare&amp;st._surl=%1&amp;title=%2" style="background-position: -96px 0">'+
            '</a>'+
                '<a target="_blank" title="Поделиться в Моем Мире@Mail.Ru" href="http://connect.mail.ru/share?url=%1&amp;title=%2" style="background-position: -128px 0">'+
            '</a>'+
                '<a target="_blank" title="Опубликовать в Blogger.com" href="http://www.blogger.com/blog_this.pyra?t&amp;u=%1&amp;n=%1" style="background-position: -160px 0"'+
            '</a>'+
                '<a target="_blank" title="Сохранить закладку в Google" href="http://www.google.com/bookmarks/mark?op=edit&amp;output=popup&amp;bkmk=%1&amp;title=%2" style="background-position: -192px 0" >'+
            '</a>'+
            '   <a target="_blank" title="Добавить в Яндекс.Закладки" href="http://zakladki.yandex.ru/newlink.xml?url=%1&amp;name=%2" style="background-position: -224px 0" >'+
            '</a>'+
        '</span>'+
    '</div>',
        actions:[{
            clazz:'delete',
            name:'deleteEl'
        }],
        resizable:false,
        onlyOne:true
    },
    'el-anchor':{
        html:'<div><img src="/static/img/elements-min/anchor-min.png"/><span style="text-align:center;"><b id="text-anchor"></b></span></div>',
        actions:[{
            clazz:'delete',
            name:'deleteEl'
        },{
           clazz:'edit',
           name:'editAnchor'
        }],
        unpack:'unpackAnchor'
    }
};
