'use strict'
//-----------------------------------------------------------------------------------
//                       UNPACK FUNCTIONS
//-----------------------------------------------------------------------------------

/**
 * Functions to convert in REALLY html code. Just for FLASH elements.
 */
Prototypes.Unpacks={
    unpackAnchor:function(pos){

        return '<a name="'+this.JQueryContent.attr('data-name')+'" style="position:absolute;margin-left:'+pos.left+';margin-top:'+pos.top+';z-index:'+pos.zIndex+';">' +
            '<img style="width:1px;heigth:1px;" src=""/>' +
            '</a>';
    },
    unpackIMG:function(pos){
        var img='<img  src="'+this.JQueryContent.attr('src')+'" style="position:absolute;margin-left:'+pos.left+';margin-top:'+pos.top+';z-index:'+pos.zIndex+';width:'+pos.width+'; height:'+pos.height+';"/>';
        if(this.JQueryContent.attr('link-active')){
            return '<a target="_blank" href="'+this.JQueryContent.attr('img-href')+'">'+img+'</a>'
        }else{
            return img;
        }
    },
    unpackYouTube:function(pos){

        if(this.JQueryContent.attr('data-ruel-link')){
            return '<object style="position:absolute;margin-left:'+pos.left+
            ';margin-top:'+pos.top+
            ';z-index:'+pos.zIndex+';" id="videoPlayer" width="'+pos.width+'" height="'+pos.height+'"'+

            ' style="visibility: visible;" data="http://storage.a-jetbox.org/storage/static/swf/uppod.swf" type="application/x-shockwave-flash">'+
            '<param value="videoPlayer" name="id">'+
            '<param value="true" name="allowFullScreen">'+
            '<param value="uid=videoPlayer&m=video&file='+this.JQueryContent.attr('data-ruel-link')+($(this.JQueryContent).attr('data-autoplay')?'&auto=play':'')+'" name="flashvars">'+
            '<param value="always" name="allowScriptAccess">'+
            '<param name="wmode" value="opaque" />'+
            '<param name="AllowNetworking" value="all"></object>';
        }else{
            return '<iframe width="'+pos.width+'" height="'+pos.height+'" src="http://www.youtube.com/embed/'
            +this.JQueryContent.attr('src').split('/')[4]+
            ''+($(this.JQueryContent).attr('data-autoplay')?'?autoplay=1':'')+'" style="position:absolute;margin-left:'+pos.left+
            ';margin-top:'+pos.top+';'+
            'z-index:'+pos.zIndex+';'+
            '"  frameborder="0"allowfullscreen></iframe>';
        }
    },
    unpackAudio:function(pos){
        var start=this.JQueryContent.attr('data-onstart')=='true'?'1':'0';
        return  '<div style="position:absolute;margin-left:'+pos.left+';margin-top:'+pos.top+
        ';z-index:'+pos.zIndex+';'+'">'+
        '<object data="http://a-jetlanding.com/static/swf/player_mp3.swf" width="200" height="20">'+
        '<param name="movie" value="http://a-jetlanding.com/static/swf/player_mp3.swf" />'+
        '<param name="FlashVars" value="mp3='+this.JQueryContent.attr('data-audio')+'&amp;autoplay='+start+'&amp;'+this.JQueryContent.attr('data-decoration')+'"/>'+

        '</object></div>';
    },
    unpackHTML:function(pos){
        if(!this.JQueryContent.children().attr('data-flash')){
            return '<div style="position:absolute;margin-left:'
            +pos.left+';margin-top:'
            +pos.top+';width:'+pos.width+
            '; height:'+pos.height+
            ';z-index:'+pos.zIndex+';">'+this.JQueryContent.html()+'</div>';
        }else{
            return  '<div style="position:absolute;margin-left:'
            +pos.left+';margin-top:'
            +pos.top+';width:'+pos.width+'; height:'+pos.height+
            ';z-index:'+pos.zIndex+';">'
            +'<object type="application/x-shockwave-flash" data="'+this.JQueryContent.children().attr('data-flash')+'" width="'+pos.width+'" height="'+pos.height+'">'
            +'</object></div>';
        }
    }
};