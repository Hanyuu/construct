from .base import LOG_LEVEL
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        },
    'handlers': {
        'null': {
            'level':LOG_LEVEL,
            'class':'django.utils.log.NullHandler',
            },
        'logfile': {
            'level':LOG_LEVEL,
            'class':'logging.handlers.RotatingFileHandler',
            'filename': "./debug.log",
            'maxBytes': 10485760,
            'backupCount': 50,
            'formatter': 'standard',
            },
        'console':{
            'level':LOG_LEVEL,
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
        },
    'loggers': {
            'apps': dict(handlers=['logfile', 'console'], level=LOG_LEVEL, propagate=False),
        }
}
