"""
Django settings for cons project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from os.path import join, normpath

LOCALE_PATHS = (
    normpath(join(__file__, '../../../locale')),
)
ADMINS = (

)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'sw@9$^(79t8t*q@#^bb=-@n5(0r7d39#+*i@+m*gwn8wb9(n(z'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',


    'bootstrap3',
    'django_rq',
    'compressor',

    'apps.construct',
    'apps.utils',
    'apps.api_server',


)

##########################################################

MIDDLEWARE_CLASSES = (
    #'django.middleware.cache.UpdateCacheMiddleware',

    'django.middleware.gzip.GZipMiddleware',
    #'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'django.middleware.cache.FetchFromCacheMiddleware',

)
CORS_ORIGIN_ALLOW_ALL = True
ROOT_URLCONF = 'configs.urls'

WSGI_APPLICATION = 'configs.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

TIME_ZONE = 'Europe/Moscow'

LANGUAGE_CODE = 'ru-RU'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'
# Additional locations of static files

STATIC_ROOT = normpath(join(__file__, '../../../st'))

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    normpath(join(__file__, '../../../static')),
)
TEMPLATE_DIRS = (
    normpath(join(__file__, '../../../templates')),
)
# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder'
)

############################################### BOOTSTRAP ########################################################

BOOTSTRAP3 = {

    # The URL to the jQuery JavaScript file
    'jquery_url': '//code.jquery.com/jquery-2.1.1.min.js',

    # The Bootstrap base URL
    'base_url': '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/',

    # The complete URL to the Bootstrap CSS file (None means derive it from base_url)
    'css_url': '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css',

    # The complete URL to the Bootstrap CSS file (None means no theme)
    'theme_url': None,

    # The complete URL to the Bootstrap JavaScript file (None means derive it from base_url)
    'javascript_url': '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',

    # Put JavaScript in the HEAD section of the HTML document (only relevant if you use bootstrap3.html)
    'javascript_in_head': False,

    # Include jQuery with Bootstrap JavaScript (affects django-bootstrap3 template tags)
    'include_jquery': False,

    # Label class to use in horizontal forms
    'horizontal_label_class': 'col-md-2',

    # Field class to use in horiozntal forms
    'horizontal_field_class': 'col-md-4',

    # Set HTML required attribute on required fields
    'set_required': True,

    # Set placeholder attributes to label if no placeholder is provided
    'set_placeholder': True,

    # Class to indicate required (better to set this in your Django form)
    'required_css_class': '',

    # Class to indicate error (better to set this in your Django form)
    'error_css_class': 'has-error',

    # Class to indicate success, meaning the field has valid input (better to set this in your Django form)
    'success_css_class': 'has-success',

    # Renderers (only set these if you have studied the source and understand the inner workings)
    'formset_renderers':{
        'default': 'bootstrap3.renderers.FormsetRenderer',
    },
    'form_renderers': {
        'default': 'bootstrap3.renderers.FormRenderer',
    },
    'field_renderers': {
        'default': 'bootstrap3.renderers.FieldRenderer',
        'inline': 'bootstrap3.renderers.InlineFieldRenderer',
    },
}
###################################################################################################################


######################################### REDIS ###################################################################
REDIS_PASSWORD=''#'fkg7h4f3f5'
REDIS_HOST='localhost'
REDIS_PORT=6379
REDIS_DB=4


######################################### SESSSION & AUTH #########################################################

'''SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_PREFIX = 'session_'''
SESSION_SAVE_EVERY_REQUEST = True
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
AUTHENTICATION_BACKENDS = (
    'apps.users.backends.APIAuthBackend',
    'django.contrib.auth.backends.ModelBackend',

)
###################################################################################################################

######################## CACHE ####################################################################################
'''CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '%s:%s' % (REDIS_HOST, REDIS_PORT),
        'OPTIONS': { # optional
            'DB': REDIS_DB,
            'PASSWORD': REDIS_PASSWORD,
            'MAX_CONNECTIONS': 50,
        },
    },
}'''
###################################################################################################################

###################################### DANGO RQ ###################################################################
RQ_QUEUES = {
    'default': {
        'HOST': REDIS_HOST,
        'PORT': REDIS_PORT,
        'DB': REDIS_DB,
        'PASSWORD': REDIS_PASSWORD,
        'DEFAULT_TIMEOUT': 360,
    },
}
RQ_SHOW_ADMIN_LINK = True
###################################################################################################################


HOSTING_PATH='/home/django/projects/a-jethosting/' #
HOSTING_DOMAIN = 'ajetlp.com'
DOMAIN_TEMPLATE='%(user)s-%(capture)s.%(domain)s'

DISK_PATH_TEMPLATE='%(hosting)s/%(domain)s/%(user)s/%(capture)s/'

PATH_TO_WKHTMLIMAGE = '/usr/local/bin/wkhtmltoimage'

IS_LOCAL_MODE = True

LOG_LEVEL='DEBUG'



