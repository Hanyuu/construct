from django.conf.urls import patterns, include, url
from django.contrib import admin
from apps.construct.urls import urlpatterns as construct_urls
from apps.api_server.urls import router
from apps.api_server.urls import api_patterns
from django.http.response import HttpResponse


admin.autodiscover()





urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^django-rq/', include('django_rq.urls')),
    url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", content_type="text/plain")),
)

urlpatterns += construct_urls

urlpatterns += router.urls

urlpatterns += api_patterns


